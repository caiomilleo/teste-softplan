import React from 'react';
import { ApolloClient, InMemoryCache } from '@apollo/client';
import { ApolloProvider } from '@apollo/client';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Countries from './pages/Countries/Countries';
import Country from './pages/Country/Country';

const client = new ApolloClient({
  uri: 'https://countries-274616.ew.r.appspot.com',
  cache: new InMemoryCache(),
});
function App() {
  return (
    <ApolloProvider client={client}>
      <Router>
        <Switch>
          <Route exact path={['/', '/countries']}>
            <Countries />
          </Route>
          <Route exact path='/country/:id'>
            <Country />
          </Route>
        </Switch>
      </Router>
    </ApolloProvider>
  );
}

export default App;
