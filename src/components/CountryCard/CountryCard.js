import React from 'react';
import { useHistory } from 'react-router-dom';
import { Container } from './styles';

function CountryCard({ country }) {
  const history = useHistory();
  const handleGoToCountryPage = (id) => history.push(`/country/${id}`);
  return (
    <Container onClick={() => handleGoToCountryPage(country._id)}>
      <div className='country-flag'>
        <div className='inner'>
          {country && country.flag && country.flag && country.flag.svgFile && (
            <img src={country.flag.svgFile} alt='flag' className='img-flag' />
          )}
        </div>
      </div>

      <div className='country-info'>
        <span className='country-name'>{country.name}</span>
        <span className='country-name'>{country.capital}</span>
      </div>
    </Container>
  );
}

export default CountryCard;
