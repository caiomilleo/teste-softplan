import styled from 'styled-components';

export const Container = styled.div`
  background-color: #f7f7f7;
  border-radius: 3px;
  border: 1px solid #ddd;
  box-shadow: 0 2px 4px #e6e6e6;
  cursor: pointer;
  margin: 0 1em 1.5em 1em;
  position: relative;
  flex-grow: 1;
  flex: 1 1 300px;

  .country-flag {
    display: flex;
    width: 100%;
    justify-content: center;
    align-items: center;
    overflow: hidden;
    height: 200px;
    max-height: 200px;
    background-color: gainsboro;
    .inner {
      display: table-cell;
      vertical-align: middle;
    }
    .img-flag {
      width: 200px;
    }
  }
  .country-info {
    display: flex;
    flex-direction: column;
    background: #fff;

    .country-name {
      transition: all 0.2s ease-in-out;
      text-align: center;
      display: block;
      background-color: #f7f7f7;
      border-radius: 3px 3px 0 0;
      font-weight: bold;
      line-height: 1.5em;
      padding: 0.5em 0.75em;
    }
  }
`;
