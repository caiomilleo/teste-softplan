import React from 'react';
import { Container } from './styles';

export function InputComponent({
  label,
  placeholder,
  value,
  setValue,
  required,
  type,
  disabled = false,
}) {
  return (
    <Container>
      {label && <label>{label}</label>}
      <input
        placeholder={placeholder}
        value={value}
        className='input'
        required={required}
        type={type}
        disabled={disabled}
        onChange={(e) => setValue(e.target.value)}
      ></input>
    </Container>
  );
}
