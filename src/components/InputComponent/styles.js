import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  label {
    display: flex;
    font-style: normal;
    font-weight: bold;
    font-size: 16;
    line-height: 25px;
  }
  input {
    width: 282px;
    height: 50px;
    margin-top: 10px;
    box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.06);
    border-radius: 2px;
    border: none;
  }
`;
