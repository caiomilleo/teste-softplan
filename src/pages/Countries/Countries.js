import React, { useEffect, useState } from 'react';
import { useLazyQuery } from '@apollo/client';
import CountryCard from '../../components/CountryCard/CountryCard';
import { InputComponent } from '../../components/InputComponent/InputComponent';
import useDebounce from '../../utils/hooks/useDebounce';
import { GET_COUNTRIES } from '../queries';
import { Container } from './styles';

function Countries() {
  const [first, setFirst] = useState(10);
  const [searchValue, setSearchValue] = useState('');
  const debouncedSearch = useDebounce(searchValue, 1000);
  const [getCountries, { loading, data }] = useLazyQuery(GET_COUNTRIES);

  const handleLoadMore = () => setFirst(first + 10);

  useEffect(() => {
    getCountries({
      variables: {
        first: first,
      },
    });
  }, [getCountries, first]);

  useEffect(() => {
    if (debouncedSearch)
      getCountries({
        variables: {
          filter: {
            name_contains: debouncedSearch,
          },
        },
      });
  }, [getCountries, debouncedSearch]);

  if (loading) return <p>Loading...</p>;

  return (
    <Container>
      <div className='search-container'>
        <InputComponent
          value={searchValue}
          setValue={setSearchValue}
          placeholder={'Buscar países'}
        />
      </div>

      <div className='countries-container'>
        {data &&
          data.Country &&
          data.Country.length > 0 &&
          data.Country.map((item) => (
            <CountryCard country={item} key={item._id} />
          ))}
      </div>

      {data && data.Country && data.Country.length > 0 && (
        <div className='button-container'>
          <button onClick={handleLoadMore} className='button-load-more'>
            Carregar mais
          </button>
        </div>
      )}
    </Container>
  );
}
export default Countries;
