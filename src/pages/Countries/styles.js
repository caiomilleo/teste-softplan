import styled from 'styled-components';

export const Container = styled.div`
  .search-container {
    margin: 0 1em 1.5em 1em;
  }

  .search-container,
  .countries-container {
    display: flex;
    flex-flow: row wrap;
    padding-top: 30px;
    flex-wrap: wrap;
  }

  .button-container {
    display: flex;
    justify-content: center;
    padding-top: 30px;
    padding-bottom: 30px;

    .button-load-more {
      cursor: pointer;
      border: none;
      background: gainsboro;
      border-radius: 5px;
      height: 50px;
    }
  }
`;
