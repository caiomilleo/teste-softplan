import React, { useEffect, useState } from 'react';
import { useLazyQuery } from '@apollo/client';
import CountryCard from '../../components/CountryCard/CountryCard';
import { InputComponent } from '../../components/InputComponent/InputComponent';
import { GET_COUNTRY_BY_ID } from '../queries';
import { useParams } from 'react-router-dom';
import { Container } from './styles';

function Country() {
  let { id } = useParams();
  const [nameValue, setNameValue] = useState('');
  const [capitalValue, setCapitalValue] = useState('');
  const [getCountryById, { loading, data }] = useLazyQuery(GET_COUNTRY_BY_ID, {
    onCompleted() {
      if (data && data.Country) {
        setNameValue(data.Country[0].name);
        setCapitalValue(data.Country[0].capital);
      }
    },
  });

  useEffect(() => {
    getCountryById({
      variables: {
        _id: id,
      },
    });
  }, [getCountryById, id]);

  if (loading) return <p>Loading...</p>;

  return (
    <Container>
      {data &&
        data.Country &&
        data.Country.length > 0 &&
        data.Country.map((item) => (
          <div key={item._id}>
            <CountryCard country={item} key={item._id} />
            <div className='form-container'>
              <InputComponent
                label={'Nome'}
                value={nameValue}
                setValue={setNameValue}
              />
              <InputComponent
                label={'Capital'}
                value={capitalValue}
                setValue={setCapitalValue}
              />
              <InputComponent
                label={'Área'}
                value={item.area}
                disabled={true}
              />
              <InputComponent
                label={'População'}
                value={item.population}
                disabled={true}
              />
              <InputComponent
                label={'Top Level Domain'}
                value={item.topLevelDomains[0].name}
                disabled={true}
              />
            </div>
          </div>
        ))}
    </Container>
  );
}
export default Country;
