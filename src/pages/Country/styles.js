import styled from 'styled-components';

export const Container = styled.div`
  .form-container {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    margin: 0 1em 1.5em 1em;
  }
`;
