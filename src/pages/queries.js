import { gql } from '@apollo/client';

export const GET_COUNTRIES = gql`
  query Country($first: Int, $offset: Int, $filter: _CountryFilter) {
    Country(first: $first, offset: $offset, filter: $filter) {
      _id
      name
      capital
      flag {
        svgFile
      }
    }
  }
`;

export const GET_COUNTRY_BY_ID = gql`
  query Country($_id: String) {
    Country(_id: $_id) {
      _id
      name
      capital
      area
      population
      topLevelDomains {
        name
      }
      flag {
        svgFile
      }
    }
  }
`;
